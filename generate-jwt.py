#!/usr/bin/env python

import sys
import json
import jwt

# HS512, RSA512
ALGORITHM = 'RSA512'
SECRET    = 'testtesttest'
PRIV_KEY = 'MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDBGzt361EC4riygMie4qnnWVNNNdY3yiRdHf+BIeIgH6ru9l4AZYDxQuMJi4QIt17y1Lo7HNnBKP+gzSRkFe4cpira6RYqNMSY7CWlBbi8GlfrRtzHmtfCgSNtxaCEr2hVbBjg7BsAnhNiSWRQdkLlu5D7xFTn27oUhZK/Y3cP1UGR9iwe+nc7hpdVoTx1nR3GV1R2/9Z4+guQJ5RiAuwbIOD/Oxm8OtfX7K82yM02YH+SwlKim0y/0J/JSlJex+i/7waEEK2DMO+CffaTDv3A+ytOSCcLQNG26/7LeoJdf5knpxsnfW1W6z0Bp1SfYKtYm6vr8pFiyeJBoSd8DjeHAgMBAAECggEATcnKiuQgHil2fXGjX12vU6KIC/JD+PVU8K3E3x/nF0mqcdiMxVweNRHco1uwNFKpzOgknOtiTI1XbE82+Zpb8O5/FPAQHbBkAbYe8/r6D8wM3poNFdsqTw/2VaHmp4yBWufQncp+liEXbeBiqIb7K9AC92h/S2cX7mxgUPigXZ+2V5Bvdjo2jAtdrq6nXFtzsqCxNKFpFifdV2TkFbq6vGB/mJdrFHspUiMZc2KAR7/pQCO/oR/WIyHRTBvWcM/zrd/7A6TZRKYMabYDn1dbyP/ATqyJ/rtx+7Josj+LogBVrdeD7USpdHj6FLbT/Z0N5o1tintUlj8eo1XqNupjAQKBgQD2VLTgiVphbju0YA65I1yWYuPYdDXJVkJjqv7lFndPdOGZsfysdc2fdkdAEkPVZhI5Tve8CvOW98WnVLkj/WRyfBVeVbekIGdsH/PWRTrLv5mXbpDGYP0z1c7FTMx6qiaE6ZLZATy4jWBLRPQcJj5rm70nO4Sajfo5TQW4BB+abQKBgQDIr7Dvq5AixiMSzbvhPIDqjIYQv+TCbzKoNm/JuRgE/1mxfgxuthNA8OLDBEmCa3SWUo9Slrq5KnynVMnxIMiTw9CSbLhUYxqlrP0ei6C7d1bv5N3tH4GD5ljCmobz04Dg56/bs/pbfJBjfE2qxtrEgzK51FazUzlFUwzf++vhQwKBgQCCUuRzWxfvLjP2RU/k0XmdsuoazbjbMYDkXEaJC27KIetttkYZNCY6tIgTtNYgo+UGXuogmaawe5wd4GwnR1uKWm2vwCftdJtjEDCgb3Yg79ihzoRYAF5Vu0uvBOe2Cilg7nabytTGSz3j9LI15mHl6rqTdCC+6Fi3UjGxXrUdrQKBgQCjPF0EJZ2A239N3TA0Hd2Saqjl2OA+0un2bwjwoPCLqBQRzkBYkRsKGKWqEMOmJLdZSmAvwlCxKoDoLoxv6MWdg4oUgABTC6VyG0pr3RDpS0HYsWVeaeEUI3oRGFYq39+VkH63iyZZ4fEkKsiV31e0vi61fqTjCg+sdsgjLCuKNwKBgQDhp8wfpcqYnvDvn7OKL52SiXfktTWe8xq6pVKLDrHScEUhj/vgcXkvOiELP+jnNivMU2AWObLq0Nf+JWpH4HrIH8R+Ag7sgy+mL53Ku+4I2et1JsHCc94AS59qY/C4AqZDgpFAFYKTNPRn+bQMAIRCh2dIayehKTgZpvhmxxSSvw=='

if len(sys.argv) > 1:
    input = sys.argv[1]
else:
    input = 'payloads/johnqpublic.json'

with open(input) as payload_file:
    payload = json.load(payload_file)
    private_key = "-----BEGIN PRIVATE KEY-----\n{}\n-----END PRIVATE KEY-----".format(PRIV_KEY)
    encoded = jwt.encode(payload, private_key, algorithm='RS512')
    #encoded = jwt.encode(payload, SECRET, algorithm='HS512')

    title = 'JWT ({})'.format(input)
    dashes = '-'*len(title);
    print '+-{}-+'.format(dashes)
    print '+ {} +'.format(title)
    print '+-{}-+'.format(dashes)
    print encoded
