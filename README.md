# jwt-generator
An easy to use script to generate JWTs quickly for repetitive development workflows.

## Usage
`./jwt-generator.sh <json file to use as payload>`

The algorithm and secret used to sign the token can be changed at the top of the script.